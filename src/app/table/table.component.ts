import { Component, OnInit } from "@angular/core";
import { StarRatingComponent } from "ng-starrating";
@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"],
})
export class TableComponent implements OnInit {
  //#region  Public Properties
  public tableData = [];
  public tableHeaders=[];
//#endregion
  constructor() {
    this.tableHeaders=["Ratings","Display","Face touch ID","Camera","Video","Chip","Apple Pencil Compatability","Connector"]
    this.tableData = [
      {
        title: "Apple iPad pro 12.9 Inch(4th Generation)",
        img: "assets/img/tab.jpg",
        display: "12.9 Inch Liquid Retina Display",
        faceId: "face ID",
        applePencil: "Apple Pencile(2nd generatiin)",
        compatability: "Compatable with smart keyboard Folio and Blutooth",
        conector: "USB-C connector",
        camera: "12MP photos",
        vidoes: "4k video recording",
        chip: "A12z Bionic chip with Nueral Engine",
      },
      {
        title: "Apple iPad pro 12.9 Inch(4th Generation)",
        img: "assets/img/tab.jpg",
        display: "12.9 Inch Liquid Retina Display",
        faceId: "face ID",
        applePencil: "Apple Pencile(2nd generatiin)",
        compatability: "Compatable with smart keyboard Folio and Blutooth",
        conector: "USB-C connector",
        camera: "12MP photos",
        vidoes: "4k video recording",
        chip: "A12z Bionic chip with Nueral Engine",
      },
      {
        title: "Apple iPad pro 12.9 Inch(4th Generation)",
        img: "assets/img/tab.jpg",
        display: "12.9 Inch Liquid Retina Display",
        faceId: "face ID",
        applePencil: "Apple Pencile(2nd generatiin)",
        compatability: "Compatable with smart keyboard Folio and Blutooth",
        conector: "USB-C connector",
        camera: "12MP photos",
        vidoes: "4k video recording",
        chip: "A12z Bionic chip with Nueral Engine",
      },
    ];
  }

  ngOnInit() {}
  //#region public methods
  onRate($event: {
    oldValue: number;
    newValue: number;
    starRating: StarRatingComponent;
  }) {
    alert(`Old Value:${$event.oldValue}, 
      New Value: ${$event.newValue}, 
      Checked Color: ${$event.starRating.checkedcolor}, 
      Unchecked Color: ${$event.starRating.uncheckedcolor}`);
  }
  //#endregion
}
